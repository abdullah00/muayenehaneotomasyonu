﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Models;

namespace WebProje.Controllers
{
  
    public class AdminController : Controller
    {
        // GET: Admin
        private HastaneEntities1 db = new HastaneEntities1();
        public ActionResult Index() 
        {
            return View();
        }
        public ActionResult DoktorlarGoruntule()
        {
            var doktor = db.Doktor.ToList();
            return View(doktor);
        }
       
        public ActionResult RandevuEkle()
        {     
            ViewBag.Randevu_Doktor = new SelectList(db.Doktor,"Doktor_ID","Doktor_Adi");
            return View();
        }
        [HttpPost]
        public ActionResult RandevuEkle(Randevu model)
        {
            if(ModelState.IsValid)
            {
                db.Randevu.Add(model);
                db.SaveChanges();
                return RedirectToAction("RandevuEkle");
            }
            ViewBag.Randevu_Doktor = new SelectList(db.Doktor, "Doktor_ID", "Doktor_Adi");
            return View();
        }
        public ActionResult DoktorEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult DoktorEkle(Doktor model)
        {
            if(ModelState.IsValid)
            {
                HttpPostedFileBase resim = Request.Files["pict"] as HttpPostedFileBase;
                string path = HttpContext.Server.MapPath("~/Content/Resimler/" + resim.FileName);
                resim.SaveAs(path);
                model.Doktor_Resim = "~/Content/Resimler/" + resim.FileName;
                db.Doktor.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            return View();
        }
      
    }
}
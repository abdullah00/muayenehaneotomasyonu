﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Models;

namespace WebProje.Controllers
{
    public class ProfilimController : Controller
    {
        HastaneEntities1 baglanti = new HastaneEntities1();

        public ActionResult Index()
        {
            int id = Convert.ToInt32(Session["Uye"]);
            var uye = baglanti.Uye.Find(id);


            return View(uye);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebProje.Models;

namespace WebProje.Controllers
{
    public class RandevuController : Controller
    {
        private HastaneEntities1 db = new HastaneEntities1();

        // GET: Randevu
        public ActionResult Index()
        {
            var randevu = db.Randevu.Include(r => r.Doktor);
            return View(randevu.ToList());
        }

        // GET: Randevu/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Randevu randevu = db.Randevu.Find(id);
            if (randevu == null)
            {
                return HttpNotFound();
            }
            return View(randevu);
        }

        // GET: Randevu/Create
        public ActionResult Create()
        {
            ViewBag.Randevu_Doktor = new SelectList(db.Doktor, "Doktor_ID", "Doktor_Adi");
            return View();
        }

        // POST: Randevu/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Randevu_ID,Randevu_Tarih,Randevu_Doktor")] Randevu randevu)
        {
            if (ModelState.IsValid)
            {
                db.Randevu.Add(randevu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Randevu_Doktor = new SelectList(db.Doktor, "Doktor_ID", "Doktor_Adi", randevu.Randevu_Doktor);
            return View(randevu);
        }

        // GET: Randevu/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Randevu randevu = db.Randevu.Find(id);
            if (randevu == null)
            {
                return HttpNotFound();
            }
            ViewBag.Randevu_Doktor = new SelectList(db.Doktor, "Doktor_ID", "Doktor_Adi", randevu.Randevu_Doktor);
            return View(randevu);
        }

        // POST: Randevu/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Randevu_ID,Randevu_Tarih,Randevu_Doktor")] Randevu randevu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(randevu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Randevu_Doktor = new SelectList(db.Doktor, "Doktor_ID", "Doktor_Adi", randevu.Randevu_Doktor);
            return View(randevu);
        }

        // GET: Randevu/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Randevu randevu = db.Randevu.Find(id);
            if (randevu == null)
            {
                return HttpNotFound();
            }
            return View(randevu);
        }

        // POST: Randevu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Randevu randevu = db.Randevu.Find(id);
            db.Randevu.Remove(randevu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

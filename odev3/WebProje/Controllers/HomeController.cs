﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProje.Models;

namespace WebProje.Controllers
{
    public class HomeController : Controller
    {
        HastaneEntities1 baglanti = new HastaneEntities1();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult KayitOl()
        {
            return View();
        }
        [HttpPost]
        public ActionResult KayitOl(FormCollection form)
        {
            Uye uyekayit = new Uye();
            uyekayit.Uye_Adi = form["KullaniciAdi"];
            uyekayit.Uye_Sifre = form["Sifre"];
            baglanti.Uye.Add(uyekayit);
            baglanti.SaveChanges();
            return View();
        }

        public ActionResult Cikis()
        {
            Session["Uye"] = null;
            return Redirect("~/Home");
        }
        public ActionResult GirisYap()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GirisYap(FormCollection form)
        {
            List<Uye> uyegiris = baglanti.Uye.ToList();
            foreach (Uye list in uyegiris)
            {
                if (list.Uye_Adi==form["KullaniciAdi"] && list.Uye_Sifre==form["Sifre"])
                {
                    Session["Uye"] = list.Uye_ID;
                }
            }

            return Redirect("~/Home");
        }
        public ActionResult About()
        {
       

            return View();
        }

        public ActionResult Contact()
        {
           

            return View();
        }
        public ActionResult RandevuAl(string DoktorID)
        {
            if (Session["Uye"] == null)
            {
                return Redirect("~/Home/GirisYap");
            }
            ViewBag.Doktorlar = baglanti.Doktor.ToList();
            if(DoktorID!=null)
            { 
            int doktorid = Convert.ToInt32(DoktorID);
            ViewBag.Randevular = baglanti.Randevu.Where(x => x.Randevu_Doktor == doktorid).ToList();
            ViewBag.SecilenDoktor = doktorid;
            }
            else { ViewBag.Randevular = baglanti.Randevu.ToList(); }
            return View();
        }
        [HttpPost]
        public ActionResult RandevuAl(FormCollection form)
        {
            if (Session["Uye"]==null)
            {
                return Redirect("~/Home/GirisYap");
            }
            Hasta hasta = new Hasta
            {
                Hasta_Adi = form["hastaadi"].ToString(),
                Hasta_Tc = form["tcno"].ToString(),
                Hasta_Soyadi = form["soyad"].ToString(),
                Hasta_DogumTarih =Convert.ToDateTime(form["dogumtarih"])
            };
            baglanti.Hasta.Add(hasta);
            baglanti.SaveChanges();

            RandevuKayit randevual = new RandevuKayit();
            randevual.RandevuKayit_Doktor =Convert.ToInt32(form["Doktor"]);
            randevual.RandevuKayit_Hasta = hasta.Hasta_ID;
            randevual.RandevuKayit_Tarih = Convert.ToDateTime(form["tarih"]);
            baglanti.RandevuKayit.Add(randevual);
            baglanti.SaveChanges();

            return Redirect("~/Home");
        }
    }
}